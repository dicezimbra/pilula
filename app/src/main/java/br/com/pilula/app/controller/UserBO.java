package br.com.pilula.app.controller;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.pilula.app.model.database.DataStore;

/**
 * Created by lucasferreiramachado on 05/09/15.
 */
public class UserBO {
    //facebook: {"id":"106078406434739","name":"Diego Cezimbra","email":"dicezimbra@gmail.com","gender":"male"}
    public static String getUserPictureUrl(Context act) {
        String faceinfos = DataStore.getFaceInfos(act);
        if (faceinfos == null) {
            return null;
        }
        JSONObject object = null;
        try {
            object = new JSONObject(faceinfos);

            String id = object.optString("id");
            String url = "https://graph.facebook.com/"+id+"/picture?type=normal";
            return url;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUserName(Context act) {

        String faceinfos = DataStore.getFaceInfos(act);
        if (faceinfos == null) {
            return null;
        }
        JSONObject object = null;
        try {
            object = new JSONObject(faceinfos);

            String name = object.optString("name");
            return name;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getIdade(Context act) {
        String faceinfos = DataStore.getFaceInfos(act);
        if (faceinfos == null) {
            return null;
        }
        JSONObject object = null;
        try {
            object = new JSONObject(faceinfos);

            String name = object.optString("birthday");
            return name;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
