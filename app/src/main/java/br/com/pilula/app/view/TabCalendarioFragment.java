package br.com.pilula.app.view;

import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import br.com.pilula.app.R;
import br.com.pilula.app.model.adapter.CalendarioItensAdapter;
import br.com.pilula.app.model.adapter.NotificationsAdapter;

public class TabCalendarioFragment extends FragmentTemplate {

    public static TabCalendarioFragment newInstance() {
        TabCalendarioFragment fragment = new TabCalendarioFragment();
        return fragment;
    }

    public TabCalendarioFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tab_search_item, container, false);
        setupUI(view, inflater);
        return view;
    }

    View gview;
    private RecyclerView rvList;
    public CalendarioItensAdapter adapter;
    List<View> jsonObjects = new ArrayList<>();
    private void setupUI(View view, LayoutInflater inflater) {
        gview= view;
      /*  rvList = (RecyclerView) view.findViewById(R.id.rvList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 300;
            }
        };
        rvList.setLayoutManager(linearLayoutManager);

        jsonObjects.clear();

        adapter = new CalendarioItensAdapter(getActivity(), jsonObjects );
        rvList.setAdapter(adapter);
        rvList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //FeedContextMenuManager.getInstance().onScrolled(recyclerView, dx, dy);
            }
        });

        adapter.updateItems();
*/
        loadData(inflater);
    }

    private void loadData(LayoutInflater inflater) {
        LinearLayout content = (LinearLayout) gview.findViewById(R.id.content);
        for (int i = 0; i < 3; i++) {


            View rootView = inflater.inflate(R.layout.month_layout, null, false);
            CalendarView cv = ((CalendarView)rootView.findViewById(R.id.calendar_view));
            cv.updateCalendar(new Date(), 7);
            content.addView(rootView);

        }

      /*

      int cod = -999;
               JSONObject obj = null;
               try {
                   obj = new JSONObject(result);
                   obj = obj.getJSONObject("ret");
                   cod = obj.getInt("cod");

                   if (cod == 0) {
                       JSONArray msg = obj.getJSONArray("msg");
                       jsonObjects.clear();

                       if (msg.length() == 0) {
                           showError(gView, getString(R.string.dont_have_notifications));
                           return;
                       }

                       for (int i = 0; i < msg.length(); i++) {
                           jsonObjects.add(msg.getJSONObject(i));
                       }

                       saveLastData();
                   }

               } catch (Exception e) {
                   e.printStackTrace();
                   return;
               }

               adapter.notifyDataSetChanged();

       */
    }

    public interface returnToSearchScreen {
        public void sendToScreen(String result);
    }
}
