package br.com.pilula.app.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.LinearLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.pilula.app.R;
import br.com.pilula.app.model.utils.Utils;
import br.com.pilula.app.model.adapter.HelpItemAdapter;
import butterknife.InjectView;

/**
 * Created by froger_mcs on 11.11.14.
 */
public class MainActivityHelp extends BaseTitleActivity {
    public static final String ARG_DRAWING_START_LOCATION = "arg_drawing_start_location";

    @InjectView(R.id.contentRoot)
    LinearLayout contentRoot;
    @InjectView(R.id.rvList)
    RecyclerView rvList;

    private HelpItemAdapter adapter;
    private int drawingStartLocation;

    private List<JSONObject> itens = new ArrayList<>();

    String offerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_help);

        setupToolbarBackButton();
        setupToolbarTitle(R.string.txt_help_title);

        setupUI();

        drawingStartLocation = getIntent().getIntExtra(ARG_DRAWING_START_LOCATION, 0);
        if (savedInstanceState == null) {
            contentRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    contentRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    startIntroAnimation();
                    return true;
                }
            });
        }

    }

    private void setupUI() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvList.setLayoutManager(linearLayoutManager);
        rvList.setHasFixedSize(true);

        itens.clear();


        addHelpItem(R.string.main_help_6, R.drawable.ic_event_white_24dp);
        addHelpItem(R.string.main_help_5, R.drawable.ic_notifications_white_24dp);

        adapter = new HelpItemAdapter(this, itens, new HelpItemAdapter.UsersLikedItemClickListener() {
            @Override
            public void onItemClick(JSONObject jsonObject) {


            }
        });

        rvList.setAdapter(adapter);
        rvList.setOverScrollMode(View.OVER_SCROLL_NEVER);
        rvList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    adapter.setAnimationsLocked(true);
                }
            }
        });
    }

    private void addHelpItem(int resTxt, int resImg) {
        try {
            JSONObject json = new JSONObject()
                    .put("textId", getString(resTxt))
                    .put("imgId", resImg );
            itens.add(json);
        }catch (Exception e){

        }
    }

    private void startIntroAnimation() {
        ViewCompat.setElevation(getToolbar(), 0);
        contentRoot.setScaleY(0.1f);
        contentRoot.setPivotY(drawingStartLocation);

        contentRoot.animate()
                .scaleY(1)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ViewCompat.setElevation(getToolbar(), Utils.dpToPx(8));
                        animateContent();
                    }
                })
                .start();
    }

    private void animateContent() {
        adapter.updateItems();
    }

    @Override
    public void onBackPressed() {
        ViewCompat.setElevation(getToolbar(), 0);
        contentRoot.animate()
                .translationY(Utils.getScreenHeight(this))
                .setDuration(200)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        MainActivityHelp.super.onBackPressed();
                        overridePendingTransition(0, 0);
                    }
                })
                .start();
    }

}
