package br.com.pilula.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import br.com.pilula.app.R;


public class CalendarView extends LinearLayout {
    // for logging
    private static final String LOGTAG = "Calendar View";

    // how many days to show, defaults to six weeks, 42 days
    private static final int DAYS_COUNT = 31;

    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";

    // date format
    private String dateFormat;

    // current displayed month
    private Calendar currentDate = Calendar.getInstance();

    //event handling
    private EventHandler eventHandler = null;

    // internal components
    private LinearLayout header;

    private TextView txtDate;
    private GridView grid;

    // seasons' rainbow
    int[] rainbow = new int[]{
            R.color.summer,
            R.color.fall,
            R.color.winter,
            R.color.spring
    };

    // month-season association (northern hemisphere, sorry australia :)
    int[] monthSeason = new int[]{2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};

    public CalendarView(Context context) {
        super(context);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControl(context, attrs);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initControl(context, attrs);
    }

    /**
     * Load control xml layout
     */
    private void initControl(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.control_calendar, this);

        loadDateFormat(attrs);
        assignUiElements();
        assignClickHandlers();

        updateCalendar(events, firstPlaceboDay);
    }

    private void loadDateFormat(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CalendarView);

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(R.styleable.CalendarView_dateFormat);
            if (dateFormat == null)
                dateFormat = DATE_FORMAT;
        } finally {
            ta.recycle();
        }
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = (LinearLayout) findViewById(R.id.calendar_header);

        txtDate = (TextView) findViewById(R.id.calendar_date_display);
        grid = (GridView) findViewById(R.id.calendar_grid);
    }

    private void assignClickHandlers() {

        // long-pressing a day
        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> view, View cell, int position, long id) {
                // handle long-press
                if (eventHandler == null)
                    return false;

                eventHandler.onDayLongPress((Date) view.getItemAtPosition(position));
                return true;
            }
        });
    }

    /**
     * Display dates correctly in grid
     */

    /**
     * Display dates correctly in grid
     */

    Date events;
    int firstPlaceboDay;
    public void updateCalendar(Date _events, int _firstPlaceboDay) {
        events = _events;
        firstPlaceboDay = _firstPlaceboDay;
        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar) currentDate.clone();

        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        // fill cells
        while (cells.size() < DAYS_COUNT) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // update grid
        grid.setAdapter(new CalendarAdapter(getContext(), cells, events, firstPlaceboDay));

        // update title
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        txtDate.setText(sdf.format(currentDate.getTime()));

        // set header color according to current season
        int month = currentDate.get(Calendar.MONTH);
        int season = monthSeason[month];
        int color = rainbow[season];


    }


    private class CalendarAdapter extends ArrayAdapter<Date> {
        // days with events
        private Date firstPlaceboDay;
        private int daysOfPlacebo = 0;
        private int iNormalDays = 0;
        boolean placebo = true;

        // for view inflation
        private LayoutInflater inflater;

        public CalendarAdapter(Context _context, ArrayList<Date> _days, Date _firstPlaceboDay, int _daysOfPlacebo) {
            super(_context, R.layout.control_calendar_day, _days);
            this.firstPlaceboDay = _firstPlaceboDay;
            daysOfPlacebo = _daysOfPlacebo;
            inflater = LayoutInflater.from(_context);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            // day in question
            Date date = getItem(position);
            int day = date.getDate();
            int month = date.getMonth();
            int year = date.getYear();

            // today
            Date today = new Date();

            // inflate item if it does not exist yet
            if (view == null)
                view = inflater.inflate(R.layout.control_calendar_day, parent, false);

            // if this day has an event, specify event image
            view.setBackgroundResource(0);

            ImageView background = (ImageView) view.findViewById(R.id.background);
            TextView text = (TextView) view.findViewById(R.id.text);

            int ifirstPlaceboDay = firstPlaceboDay.getDate();
            int ifirstPlaceboMonth = firstPlaceboDay.getMonth();
            int ifirstPlaceboYear = firstPlaceboDay.getYear();

            if (day == today.getDate()) {
                // if it is today, set it to blue/bold
                text.setTypeface(null, Typeface.BOLD);
                text.setTextColor(Color.WHITE);
                background.setImageResource(R.drawable.pil_hoje);
            }
            else if (ifirstPlaceboDay > day) {
                background.setImageResource(R.drawable.pil_passado);
                text.setTextColor(Color.GRAY);
            }else{

                int lastPLaceboDay = ifirstPlaceboDay + daysOfPlacebo;
                text.setTextColor(Color.WHITE);
                if(lastPLaceboDay > day){
                    background.setImageResource(R.drawable.pil_placebo);
                }else{
                    background.setImageResource(R.drawable.pil_diaria);
                }


            }




            // clear styling
            text.setTypeface(null, Typeface.NORMAL);


            if (month != today.getMonth() || year != today.getYear()) {
                // if this day is outside current month, grey it out
                text.setTextColor(getResources().getColor(R.color.greyed_out));
            }

            // set text
            text.setText(String.valueOf(date.getDate()));

            return view;
        }
    }

    /**
     * Assign event handler to be passed needed events
     */
    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    public interface EventHandler {
        void onDayLongPress(Date date);
    }
}
