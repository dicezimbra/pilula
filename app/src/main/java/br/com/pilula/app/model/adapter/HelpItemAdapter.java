package br.com.pilula.app.model.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import br.com.pilula.app.R;
import br.com.pilula.app.model.utils.Utils;
import br.com.pilula.app.model.utils.RoundedTransformation;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by froger_mcs on 11.11.14.
 */
public class HelpItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int lastAnimatedPosition = -1;
    private int avatarSize;

    private boolean animationsLocked = false;
    private boolean delayEnterAnimation = true;

    private UsersLikedItemClickListener offerLikePersonItemClickListener;

    public interface UsersLikedItemClickListener {
        void onItemClick(JSONObject jsonObject);
    }


    private boolean showLoadingView = false;
    private int loadingViewSize = Utils.dpToPx(200);

    private List<JSONObject> itens;

    public HelpItemAdapter(Context context, List<JSONObject> itens, UsersLikedItemClickListener offerLikePersonItemClickListener) {
        this.context = context;
        avatarSize = context.getResources().getDimensionPixelSize(R.dimen.comment_avatar_size);
        this.itens = itens;
        this.offerLikePersonItemClickListener = offerLikePersonItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.new_item_help, parent, false);
        return new LikePersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        LikePersonViewHolder holder = (LikePersonViewHolder) viewHolder;

        final JSONObject jsonObject = itens.get(position);
        holder.tvText.setText(jsonObject.optString("textId"));
        int resId = jsonObject.optInt("imgId");

        Picasso.with(context)
                .load(resId)
                .transform(new RoundedTransformation())
                .into(holder.ivUserAvatar);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(offerLikePersonItemClickListener!=null){
                    offerLikePersonItemClickListener.onItemClick(jsonObject);
                }

            }
        };

        holder.itemView.setOnClickListener(onClickListener);
        holder.tvText.setOnClickListener(onClickListener);
        holder.ivUserAvatar.setOnClickListener(onClickListener);
    }

    private void runEnterAnimation(View view, int position) {
        if (animationsLocked) return;

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(100);
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(1.f)
                    .setStartDelay(delayEnterAnimation ? 20 * (position) : 0)
                    .setInterpolator(new DecelerateInterpolator(2.f))
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animationsLocked = true;
                        }
                    })
                    .start();
        }
    }

    @Override
    public int getItemCount() {
        if(this.itens!=null){
            return this.itens.size();
        }
        return 0;
    }

    public void updateItems() {
        notifyDataSetChanged();
    }

    public void addItem() {
        notifyItemInserted(getItemCount() - 1);
    }

    public void setAnimationsLocked(boolean animationsLocked) {
        this.animationsLocked = animationsLocked;
    }

    public void setDelayEnterAnimation(boolean delayEnterAnimation) {
        this.delayEnterAnimation = delayEnterAnimation;
    }

    public static class LikePersonViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.ivUserAvatar)
        ImageView ivUserAvatar;
        @InjectView(R.id.tvText)
        TextView tvText;

        public LikePersonViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
