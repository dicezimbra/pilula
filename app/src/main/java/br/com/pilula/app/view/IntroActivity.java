package br.com.pilula.app.view;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import br.com.pilula.app.R;
import br.com.pilula.app.model.adapter.IntroAdapter;
import br.com.pilula.app.model.adapter.IntroPageTransformer;
import br.com.pilula.app.model.log.AppLog;

public class IntroActivity extends ActionBarActivity {

    private ViewPager mViewPager;
    IntroAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        // Set an Adapter on the ViewPager
        adapter = new IntroAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);

        // Set a PageTransformer
        mViewPager.setPageTransformer(false, new IntroPageTransformer());

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int oldPositionOffsetPixels = 0;
            int selected = 0;
            int oldselected =0;
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                AppLog.e(" selected = "+selected );

                if(oldselected == selected){
                    return;
                }
                if (selected == 2 && positionOffset == 0.0 && positionOffsetPixels == 0) {
                    AppLog.e("FIMMMMMMMMMMMMMMMMM");
                }
                oldselected = selected;
            }

            @Override
            public void onPageSelected(int position) {
                selected = position;
                AppLog.e("onPageSelected position = "+position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



}
