package br.com.pilula.app.view;


import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import org.json.JSONObject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import br.com.pilula.app.controller.AppConf;
import br.com.pilula.app.R;
import br.com.pilula.app.model.log.AppLog;
import br.com.pilula.app.model.database.DataStore;
import br.com.pilula.app.model.utils.FacebookUtil;
import butterknife.InjectView;

public class LoginActivity extends InitialActivityTemplate implements
        View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ServerAuthCodeCallbacks {


    @InjectView(R.id.authButton2)
    Button authButton2;


    LoginButton authButton;
    private boolean process = false;

    private static CallbackManager fbCallbackMannager = null;


    /*********************************************************************
     * GOOGLE PARAMS
     *********************************************************************/

    private GoogleApiClient mGoogleApiClient;
    private Button mSignInButton;
    private boolean mSignInClicked;
    private boolean mIntentInProgress;
    private final AtomicBoolean mServerAuthCodeRequired = new AtomicBoolean(false);
    private static final String TAG = "SignInActivity";
    private static final int REQUEST_CODE_SIGN_IN = 1;
    private static final String KEY_NEW_CODE_REQUIRED = "codeRequired";
    private static final String KEY_SIGN_IN_CLICKED = "signInClicked";
    private static final String KEY_INTENT_IN_PROGRESS = "intentInProgress";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_login);



        try {
            FacebookUtil.LFMGenerateKeyHash(this, getClass());
            FacebookSdk.sdkInitialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        fbCallbackMannager = CallbackManager.Factory.create();
        Log.e("LoginActivity", "SHA1 = " + getCertificateSHA1Fingerprint(this));
        setupUI();

        if (savedInstanceState == null) {
            pendingIntroAnimation = true;
        } else {
            //feedAdapter.updateItems(false);
        }

        /*********************************************************************
         *
         * GOOGLE INITIALIZATION
         *********************************************************************/
        mGoogleApiClient = buildGoogleApiClient(true);
        mSignInButton = (Button) findViewById(R.id.sign_in_button);

        mSignInButton.setOnClickListener(this);
    }


    private void setupUI() {

        this.authButton = new LoginButton(this);
        authButton.setReadPermissions(Arrays.asList("public_profile,email"));
        //UIUtils.textViewUnderlinedText(tvPrivacy, Html.fromHtml(getString(R.string.know_our_terms_of_use)));


        authButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("setOnClickListener", ".... process =" + process + " userID=" + userID);

                if (process == false) {
                    process = true;

                   if(!checkForCredentials()){
                        authButton.callOnClick();
                    }
                }

            }
        });

        authButton.registerCallback(fbCallbackMannager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Log.e("LoginFragment", "FacebookCallback onSuccess..." + loginResult);
                try {
                    GraphRequest request = GraphRequest.newMeRequest(
                            loginResult.getAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {

                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {

                                    //Log.e("LoginFragment", "object = " + object);
                                    String name = object.optString("name");
                                    String userid = object.optString("id");

                                    AppLog.e("LoginActivity setupUI registerCallback object = " + object.toString());
                                    if (name == null) {
                                        process = false;


                                        return;
                                    }
                                    if (userid == null) {
                                        process = false;
                                        return;
                                    }


                                    try {

                                        DataStore.setFaceInfos(LoginActivity.this, object.toString());
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                        finish();
                                        process = false;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        process = false;


                                    }
                                }
                            });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,gender,birthday");
                    request.setParameters(parameters);
                    request.executeAsync();

                } catch (Exception e) {
                    e.printStackTrace();
                    process = false;
                    showError(getString(R.string.login_fail));


                }
            }

            @Override
            public void onCancel() {
                process = false;

                showError(getString(R.string.login_fail));
            }

            @Override
            public void onError(FacebookException error) {
                //Log.e("LoginFragment", "onError error  = " + error);
                process = false;


                showError("Erro: " + error.getLocalizedMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SIGN_IN) {
            mIntentInProgress = false; //Previous resolution intent no longer in progress.

            if (resultCode == RESULT_OK) {

                if (!mGoogleApiClient.isConnected()) {

                    mGoogleApiClient.reconnect();
                }
            } else {
                mSignInClicked = false; // No longer in the middle of resolving sign-in errors.
                showError(getString(R.string.login_fail));
                if (resultCode == RESULT_CANCELED) {

                } else {

                    Log.w(TAG, "Error during resolving recoverable error.");
                }
            }
        }
        if (requestCode == 64206) {
            if (fbCallbackMannager != null)
                fbCallbackMannager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(KEY_NEW_CODE_REQUIRED, mServerAuthCodeRequired.get());
        outState.putBoolean(KEY_SIGN_IN_CLICKED, mSignInClicked);
        outState.putBoolean(KEY_INTENT_IN_PROGRESS, mIntentInProgress);
    }

    private String getCertificateSHA1Fingerprint(Context mContext) {
        PackageManager pm = mContext.getPackageManager();
        String packageName = mContext.getPackageName();
        int flags = PackageManager.GET_SIGNATURES;
        PackageInfo packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        android.content.pm.Signature[] signatures = packageInfo.signatures;
        byte[] cert = signatures[0].toByteArray();
        InputStream input = new ByteArrayInputStream(cert);
        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X509");
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        X509Certificate c = null;
        try {
            c = (X509Certificate) cf.generateCertificate(input);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        String hexString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(c.getEncoded());
            hexString = byte2HexFormatted(publicKey);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }
        return hexString;
    }

    public static String byte2HexFormatted(byte[] arr) {
        StringBuilder str = new StringBuilder(arr.length * 2);
        for (int i = 0; i < arr.length; i++) {
            String h = Integer.toHexString(arr[i]);
            int l = h.length();
            if (l == 1) h = "0" + h;
            if (l > 2) h = h.substring(l - 2, l);
            str.append(h.toUpperCase());
            if (i < (arr.length - 1)) str.append(':');
        }
        return str.toString();
    }


    /*********************************************************************
     * GOOGLE METHODS
     *********************************************************************/


    private GoogleApiClient buildGoogleApiClient(boolean useProfileScope) {
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this);

        String serverClientId = AppConf.CLIENT_ID;

        if (!TextUtils.isEmpty(serverClientId)) {
            // builder.requestServerAuthCode(serverClientId, this);
        }

        if (useProfileScope) {
            builder.addApi(Plus.API)
                    .addScope(new Scope("profile"));
        } else {
            builder.addApi(Plus.API, Plus.PlusOptions.builder()
                    .addActivityTypes().build())
                    .addScope(Plus.SCOPE_PLUS_LOGIN);
        }

        return builder.build();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:



                if (!checkForCredentials() && !mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                    mSignInClicked = true;
                    mGoogleApiClient.connect();
                }
                break;
        }
    }

    private boolean checkForCredentials() {

        String googleCredentials = DataStore.getGoogleInfos(this);
        String facebookCredentials = DataStore.getFaceInfos(this);

        if(googleCredentials != null || facebookCredentials != null){

            return true;
        }
       return false;
    }


    @Override
    public CheckResult onCheckServerAuthorization(String idToken, Set<Scope> scopeSet) {
        if (mServerAuthCodeRequired.get()) {
            Set<Scope> scopes = new HashSet<>();


            scopes.add(new Scope("profile"));

            scopes.add(Plus.SCOPE_PLUS_LOGIN);


            // also emulate the server asking for an additional Drive scope.

            return CheckResult.newAuthRequiredResult(scopes);
        } else {
            return CheckResult.newAuthNotRequiredResult();
        }
    }

    @Override
    public boolean onUploadServerAuthCode(String idToken, String serverAuthCode) {
        Log.d(TAG, "upload server auth code " + serverAuthCode + " requested, faking success");
        mServerAuthCodeRequired.set(false);
        return true;
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

        String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

        if (person != null) {
            AppLog.e("LOGINACTIVITY CADASTRO GOOGLE EMAIL = "+email);

            DataStore.setGoogleInfos(this, email + ":infos:" +person);
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();

        } else {

        }
        updateButtons();
        mSignInClicked = false;
    }

    @Override
    public void onConnectionSuspended(int cause) {

        mGoogleApiClient.connect();
        updateButtons();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {

        if (!mIntentInProgress && mSignInClicked) {
            if (result.hasResolution()) {
                try {
                    result.startResolutionForResult(this, REQUEST_CODE_SIGN_IN);
                    mIntentInProgress = true;
                } catch (IntentSender.SendIntentException e) {
                    mIntentInProgress = false;
                    AppLog.e(TAG, "Error sending the resolution Intent, connect() again.");
                    mGoogleApiClient.connect();
                }
            } else {
                showError(getString(
                        result.getErrorCode()));
            }
        }
        updateButtons();
    }


    private void updateButtons() {
        if (mGoogleApiClient.isConnecting()) {

            AppLog.e("LoginActivity", " Sign In in progress");

        } else if (mGoogleApiClient.isConnected()) {
            AppLog.e("LoginActivity", " Signed in already");
            AppLog.e("LoginActivity", " mSignInStatus will be updated when we get the user name");

        } else {
            AppLog.e("LoginActivity", "Sign In failed,");

        }
    }



}