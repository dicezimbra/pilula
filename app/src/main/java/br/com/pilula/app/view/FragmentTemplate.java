package br.com.pilula.app.view;

import android.view.View;

import br.com.pilula.app.model.utils.UIUtils;

/**
 * Created by Diego Cezimbra on 9/26/2015.
 */
public class FragmentTemplate extends android.support.v4.app.Fragment {

    protected void showError(View view, String msg) {
        UIUtils.showMsgSnackbar(view, msg);
    }
}
