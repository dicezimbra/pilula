package br.com.pilula.app.model.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import org.json.JSONObject;
import java.util.List;

import br.com.pilula.app.R;
import butterknife.ButterKnife;

public class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int lastAnimatedPosition = -1;
    private int avatarSize;

    private boolean animationsLocked = false;
    private boolean delayEnterAnimation = true;

    private NotificationsItemClickListener onItemClickListener;

    public interface NotificationsItemClickListener {
        void onItemClick(JSONObject jsonObject);

        void onItemIconClick(JSONObject jsonObject);
    }


    private List<JSONObject> itens;

    public NotificationsAdapter(Context context, List<JSONObject> itens, NotificationsItemClickListener onItemClickListener) {
        this.context = context;
        avatarSize = context.getResources().getDimensionPixelSize(R.dimen.comment_avatar_size);
        this.itens = itens;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.new_item_notification, parent, false);
        return new LikePersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        runEnterAnimation(viewHolder.itemView, position);
        LikePersonViewHolder holder = (LikePersonViewHolder) viewHolder;

        final JSONObject jsonObject = itens.get(position);



    }

    private void runEnterAnimation(View view, int position) {
        if (animationsLocked) return;

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(100);
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(1.f)
                    .setStartDelay(delayEnterAnimation ? 20 * (position) : 0)
                    .setInterpolator(new DecelerateInterpolator(2.f))
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animationsLocked = true;
                        }
                    })
                    .start();
        }
    }

    @Override
    public int getItemCount() {
        if (this.itens != null) {
            return this.itens.size();
        }
        return 0;
    }

    public void updateItems() {
        notifyDataSetChanged();
    }

    public void addItem() {
        notifyItemInserted(getItemCount() - 1);
    }

    public void setAnimationsLocked(boolean animationsLocked) {
        this.animationsLocked = animationsLocked;
    }

    public void setDelayEnterAnimation(boolean delayEnterAnimation) {
        this.delayEnterAnimation = delayEnterAnimation;
    }

    public static class LikePersonViewHolder extends RecyclerView.ViewHolder {


        public LikePersonViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
