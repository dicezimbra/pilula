package br.com.pilula.app.model.view;

import android.widget.Button;

/**
 * Created by diego on 10/08/2015.
 */
public class MCategoriesButtons {
    private String name;
    private Button button;
    private boolean clicked;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }
}
