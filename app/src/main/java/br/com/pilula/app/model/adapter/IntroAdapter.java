package br.com.pilula.app.model.adapter;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.pilula.app.view.IntroFragment;

/**
 * Created by diego on 05/05/2016.
 */
public class IntroAdapter extends FragmentPagerAdapter {

    public IntroAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return IntroFragment.newInstance(Color.parseColor("#FF4181"), position); // blue
            case 1:
                return IntroFragment.newInstance(Color.parseColor("#FF4181"), position); // pink
            default:
                return IntroFragment.newInstance(Color.parseColor("#FF4181"), position); // green
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

}
