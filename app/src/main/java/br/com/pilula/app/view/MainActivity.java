package br.com.pilula.app.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import br.com.pilula.app.R;
import br.com.pilula.app.controller.UserBO;
import br.com.pilula.app.model.log.AppLog;
import br.com.pilula.app.model.utils.CircleTransformation;
import br.com.pilula.app.model.utils.UIUtils;
import br.com.pilula.app.model.view.RevealBackgroundView;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Miroslaw Stanek on 14.01.15.
 */
public class MainActivity extends BaseActivity implements RevealBackgroundView.OnStateChangeListener {
    public static final String ARG_REVEAL_START_LOCATION = "reveal_start_location";

    private static final int USER_OPTIONS_ANIMATION_DELAY = 300;
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();
    public static final String PARAM_SELECTED_TAB = "selectedTab";

    @InjectView(R.id.content)
    CoordinatorLayout clContent;

    @InjectView(R.id.vRevealBackground)
    RevealBackgroundView vRevealBackground;

    @InjectView(R.id.tlUserProfileTabs)
    TabLayout tlUserProfileTabs;

    @InjectView(R.id.pager)
    ViewPager viewPager;

    @InjectView(R.id.ivUserProfilePhoto)
    ImageView ivUserProfilePhoto;

    @InjectView(R.id.tvProfileName)
    TextView tvProfileName;
    @InjectView(R.id.tvProfileUsername)
    TextView tvProfileUsername;
    @InjectView(R.id.tvProfileDescription)
    TextView tvProfileDescription;
    @InjectView(R.id.btnFollow)
    Button btnFollow;

    private MenuItem menuItemHelp;
    private MenuItem menuItemFilter;



    @OnClick(R.id.btnFollow)
    public void onProfileButtonClick() {


        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menuItemHelp = menu.findItem(R.id.action_help);
        menuItemHelp.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(MainActivity.this, MainActivityHelp.class));
                return true;
            }
        });
        return true;
    }

    public static void startUserProfileFromLocation(int[] startingLocation, Activity startingActivity) {
        Intent intent = new Intent(startingActivity, MainActivity.class);
        intent.putExtra(ARG_REVEAL_START_LOCATION, new int[0][10]);
        startingActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity_main);


        Toolbar tolbar= (Toolbar) findViewById(R.id.toolbar);
        tolbar.setTitle("saasdasasdaasdaasdsda");

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.img_toolbar_logo);
        AppLog.e("MAINACTIVITY ONCREATE");

        if (this.getIntent().hasExtra(ARG_REVEAL_START_LOCATION) == false) {
            int[] startingLocation = {0, 0};
            this.getIntent().putExtra(ARG_REVEAL_START_LOCATION, startingLocation);
        }


        if (ivLogo != null)
            ivLogo.setVisibility(View.GONE);


        int avatarSize = getResources().getDimensionPixelSize(R.dimen.user_profile_avatar_size);
        String profilePhoto = UserBO.getUserPictureUrl(this);

        tvProfileName.setText(UserBO.getUserName(this));

        if (UserBO.getIdade(this) == null || UserBO.getIdade(this).trim().equals("")) {
            tvProfileUsername.setVisibility(View.GONE);
        } else {
            tvProfileUsername.setText(UserBO.getIdade(this));
        }

        Picasso.with(this)
                .load(profilePhoto)
                .placeholder(R.drawable.ic_camera_3)
                .resize(avatarSize, avatarSize)
                .centerCrop()
                .transform(new CircleTransformation())
                .into(ivUserProfilePhoto);


        setupTabs();

        tvProfileDescription.setText("Clique abaixo para editar as informações do seu perfil");

        btnFollow.setText("Editar perfil");

        setupRevealBackground(savedInstanceState);

        btnFollow.setOnClickListener(shareOnFace);





    }

    View.OnClickListener shareOnFace = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            editarPerfil();
        }
    };

    private void editarPerfil() {

    }

    View.OnClickListener shareOnWhats = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };


    private void setupTabs() {
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tlUserProfileTabs.setTabsFromPagerAdapter(pagerAdapter);

        tlUserProfileTabs.setupWithViewPager(viewPager);

        int count = pagerAdapter.getCount();
        AppLog.e("MAIN ACTIVITY PAGER ADAPTER COUNT = " + count);
        for (int i = 0; i < count; i++) {
            tlUserProfileTabs.getTabAt(i).setIcon(pagerAdapter.icons[i]);
        }

    }

    private MyPagerAdapter pagerAdapter;

    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        public int[] icons = {
                R.drawable.ic_event_black_24dp,
                R.drawable.ic_notifications_black_24dp
        };


        TabCalendarioFragment tabCalendarioFragment;
        TabNotificationsListFragment tabNotificationsListFragment;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(final int pos) {

            switch (pos) {
                case 0: {

                    if (tabCalendarioFragment == null) {
                        tabCalendarioFragment = TabCalendarioFragment.newInstance();
                    }

                    return tabCalendarioFragment;
                }
                case 1: {

                    if (tabNotificationsListFragment == null) {
                        tabNotificationsListFragment = TabNotificationsListFragment.newInstance();
                    }

                    return tabNotificationsListFragment;
                }
                default: {
                    return new Fragment();
                }
            }
        }

        private TabNotificationsListFragment.TabStoreNotificationsListListener tabStoreNotificationsListListener = new TabNotificationsListFragment.TabStoreNotificationsListListener() {
            @Override
            public void onItemClick(JSONObject jsonObject) {
                handleNotificationTextClick(jsonObject);
            }

            @Override
            public void onItemIconClick(JSONObject jsonObject) {
                handleNotificationTextClick(jsonObject);
            }
        };

        public void handleNotificationTextClick(JSONObject jsonObject) {
        }

        @Override
        public int getCount() {
            return icons.length;
        }

        @Override
        public CharSequence getPageTitle(int pos) {
            return super.getPageTitle(pos);
        }
    }

    private void setupRevealBackground(Bundle savedInstanceState) {
        vRevealBackground.setOnStateChangeListener(this);
        if (savedInstanceState == null) {
            final int[] startingLocation = getIntent().getIntArrayExtra(ARG_REVEAL_START_LOCATION);
            vRevealBackground.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    vRevealBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                    vRevealBackground.startFromLocation(startingLocation);
                    return true;
                }
            });
        } else {
            vRevealBackground.setToFinishedFrame();
        }
    }

    @Override
    public void onStateChange(int state) {
        if (RevealBackgroundView.STATE_FINISHED == state) {
            viewPager.setVisibility(View.VISIBLE);
            tlUserProfileTabs.setVisibility(View.VISIBLE);
            animateUserProfileOptions();
        } else {
            tlUserProfileTabs.setVisibility(View.INVISIBLE);
            viewPager.setVisibility(View.INVISIBLE);
        }
    }

    private void animateUserProfileOptions() {
        tlUserProfileTabs.setTranslationY(-tlUserProfileTabs.getHeight());
        tlUserProfileTabs.animate().translationY(0).setDuration(300).setStartDelay(USER_OPTIONS_ANIMATION_DELAY).setInterpolator(INTERPOLATOR);
    }

    protected void showSnackBarMessage(String msg) {
        UIUtils.showMsgSnackbar(clContent, msg);
    }

    public static int selectedTab = -1;

    @Override
    protected void onResume() {
        if (selectedTab != -1) {
            int count = pagerAdapter.getCount();
            for (int i = 0; i < count; i++) {

                if (selectedTab == i) {
                    tlUserProfileTabs.getTabAt(i).select();
                }
            }
            selectedTab = -1;

        }
        super.onResume();
    }

}
