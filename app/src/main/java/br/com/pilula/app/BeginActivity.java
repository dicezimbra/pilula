package br.com.pilula.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.FacebookSdk;
import br.com.pilula.app.model.database.DataStore;
import br.com.pilula.app.model.utils.FacebookUtil;
import br.com.pilula.app.view.IntroActivity;
import br.com.pilula.app.view.MainActivity;

public class BeginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        try {
            FacebookUtil.LFMGenerateKeyHash(this, getClass());
            FacebookSdk.sdkInitialize(getApplication());
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean isUserRegistered = DataStore.getFaceInfos(BeginActivity.this) != null || DataStore.getGoogleInfos(BeginActivity.this) != null;

        if (isUserRegistered) {
            startActivity(new Intent(this, MainActivity.class));

        } else {
            startActivity(new Intent(this, IntroActivity.class));
        }
        this.finish();
    }
}