package br.com.pilula.app.model.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.com.pilula.app.R;

/**
 * Created by lucasferreiramachado on 05/09/15.
 */
public class UIUtils {

    private static ProgressDialog ringProgressDialog = null;
    public static void showDialog(Activity act, String title, String msg) {

        hideDialog();

        try {
            //Log.e(TCodes.TAG, "launchDialog");
            if (ringProgressDialog != null) {
                ringProgressDialog.dismiss();
                ringProgressDialog = null;
            }

            ringProgressDialog = ProgressDialog.show(act, title, msg, true);
            ringProgressDialog.setCancelable(true);
        }catch (Exception e){
            e.printStackTrace();
        }catch (Throwable e){
            e.printStackTrace();
        }


    }

    public static void hideDialog() {
        try {
            //Log.e(TCodes.TAG, "stopDialog");
            if (ringProgressDialog == null) {
                return;
            }
            if(ringProgressDialog.isShowing()) {
                ringProgressDialog.dismiss();
            }
            ringProgressDialog = null;
        }catch (Exception e){
            e.printStackTrace();
        }catch (Throwable e){
            e.printStackTrace();
        }


        if (globalDialog != null) {
            try {
                globalDialog.cancel();
                globalDialog.dismiss();
            } catch (Exception e) {
                globalDialog = null;
            }
        }
        globalDialog = null;
    }

    public static void showMsgSnackbar(View clContent,String msg) {
        Snackbar.make(clContent, msg, Snackbar.LENGTH_SHORT).show();
    }

    public static void showMsgSnackbar(View clContent,String msg,int duration) {
        Snackbar.make(clContent, msg, duration).show();
    }

    public static void textViewUnderlinedText(TextView textView,Spanned text) {

        textView.setText(text);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable s = new SpannableString(textView.getText());
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span: spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
        textView.setText(s);
    }

    public static void showAskDialog(Activity activity, String title, String message, final View.OnClickListener clickRight, final View.OnClickListener clickLeft) {
        dbShowDialog3(activity, title, message, clickRight, clickLeft);
    }
    public static void showAskDialog(Activity activity, int title, int message, final View.OnClickListener clickRight, final View.OnClickListener clickLeft) {
        dbShowDialog3(activity, activity.getString(title), activity.getString(message), clickRight, clickLeft);
    }


    static View.OnClickListener clickDismiss = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideDialog();
        }
    };

    private static AlertDialog globalDialog;
    static public void dbShowDialog3(final Activity act,final String title, final String message, final View.OnClickListener clickRight, final View.OnClickListener clickLeft) {
        hideDialog();

        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (globalDialog != null) {
                    hideDialog();
                    globalDialog = null;
                }

                LayoutInflater inflater = act.getLayoutInflater();
                View dialoglayout = inflater.inflate(R.layout.db_dialog3, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(act);
                builder.setView(dialoglayout);
                builder.setCancelable(false);

                globalDialog = builder.create();
                globalDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                globalDialog.show();
                Button dbDialogButton1 = (Button) dialoglayout.findViewById(R.id.db_dialog3_button1);
                Button dbDialogButton2 = (Button) dialoglayout.findViewById(R.id.db_dialog3_button2);
                TextView dbDialogText = (TextView) dialoglayout.findViewById(R.id.db_dialog3_text);

                dbDialogText.setText(message);

                if (clickLeft != null) {
                    dbDialogButton2.setOnClickListener(clickLeft);
                } else {
                    dbDialogButton2.setOnClickListener(clickDismiss);
                }

                if (clickRight != null) {
                    dbDialogButton1.setOnClickListener(clickRight);
                } else {
                    dbDialogButton1.setOnClickListener(clickDismiss);
                }
            }
        });
    }
    public static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }



}
