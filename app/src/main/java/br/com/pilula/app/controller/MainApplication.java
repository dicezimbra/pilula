package br.com.pilula.app.controller;

import android.app.Application;
import br.com.pilula.app.model.log.AppLog;
import br.com.pilula.app.model.utils.ConnectivityUtil;
import timber.log.Timber;

/**
 * Created by froger_mcs on 05.11.14.
 */
public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        try {


        }catch (Exception e){
            e.printStackTrace();
        }
/*
        // Override ConnectionClassStateChangeListener
        ConnectionClassManager.getInstance().register(new ConnectionClassManager.ConnectionClassStateChangeListener() {
            @Override
            public void onBandwidthStateChange(ConnectionQuality bandwidthState) {
                AppLog.e("InstaMaterialApplication", "onBandwidthStateChange bandwidthState = "+bandwidthState);
            }
        });
*/
        AppLog.e("InstaMaterialApplication", "onCreate");
        ConnectivityUtil.getNetworkInfo(this);
    }



}
