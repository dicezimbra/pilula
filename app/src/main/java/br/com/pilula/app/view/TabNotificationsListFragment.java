package br.com.pilula.app.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

import br.com.pilula.app.R;
import br.com.pilula.app.model.database.DataStore;
import br.com.pilula.app.model.adapter.NotificationsAdapter;


public class TabNotificationsListFragment extends FragmentTemplate implements NotificationsAdapter.NotificationsItemClickListener {

    private RecyclerView rvList;
    public NotificationsAdapter adapter;
    private ArrayList<JSONObject> jsonObjects = new ArrayList<>();
    View gView;

    @Override
    public void onItemClick(JSONObject jsonObject) {
        if(listener!=null){
            listener.onItemClick(jsonObject);
        }
    }

    @Override
    public void onItemIconClick(JSONObject jsonObject) {
        if(listener!=null){
            listener.onItemIconClick(jsonObject);
        }
    }

    private TabStoreNotificationsListListener listener;

    public void setListener(TabStoreNotificationsListListener listener) {
        this.listener = listener;
    }

    public interface TabStoreNotificationsListListener extends NotificationsAdapter.NotificationsItemClickListener {

    }

    public static TabNotificationsListFragment newInstance() {
        TabNotificationsListFragment fragment = new TabNotificationsListFragment();
        Bundle args = new Bundle();


        fragment.setArguments(args);
        return fragment;
    }

    public TabNotificationsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tab_store_offers, container, false);
        setupUI(view);
        gView = view;
        return view;
    }

    private void setupUI(View view) {

        rvList = (RecyclerView) view.findViewById(R.id.rvList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 300;
            }
        };
        rvList.setLayoutManager(linearLayoutManager);

        jsonObjects.clear();

        adapter = new NotificationsAdapter(getActivity(), jsonObjects, this );
        rvList.setAdapter(adapter);
        rvList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //FeedContextMenuManager.getInstance().onScrolled(recyclerView, dx, dy);
            }
        });

        adapter.updateItems();

        loadData();
    }

    public void removeItem(int position){
        jsonObjects.remove(position);
        adapter.notifyItemRemoved(position);
    }
    public void updateItens(){
        loadData();
    }

    private void loadData() {
      /*

      int cod = -999;
               JSONObject obj = null;
               try {
                   obj = new JSONObject(result);
                   obj = obj.getJSONObject("ret");
                   cod = obj.getInt("cod");

                   if (cod == 0) {
                       JSONArray msg = obj.getJSONArray("msg");
                       jsonObjects.clear();

                       if (msg.length() == 0) {
                           showError(gView, getString(R.string.dont_have_notifications));
                           return;
                       }

                       for (int i = 0; i < msg.length(); i++) {
                           jsonObjects.add(msg.getJSONObject(i));
                       }

                       saveLastData();
                   }

               } catch (Exception e) {
                   e.printStackTrace();
                   return;
               }

               adapter.notifyDataSetChanged();

       */
    }

    private void saveLastData() {
        int size = jsonObjects.size();
        if(size>0) {
            /*if (size > 10) {
                size = 10;
            }*/
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < size; i++) {
                jsonArray.put(jsonObjects.get(i));
            }
            DataStore.setLastNotifications(getActivity(), jsonArray.toString());
        }
    }

    private void loadLastData() {

        if(jsonObjects.size() == 0){

            String lastSearch = DataStore.getLastNotifications(getActivity());
            try {
                if(lastSearch!=null && lastSearch.length()>2) {
                    JSONArray jsonArray = new JSONArray(lastSearch);
                    int size = jsonArray.length();
                    for (int i = 0; i < size; i++) {
                        jsonObjects.add(jsonArray.getJSONObject(i));
                    }
                }
                adapter.notifyDataSetChanged();
            }catch (Exception e){

            }

        }

    }
}
