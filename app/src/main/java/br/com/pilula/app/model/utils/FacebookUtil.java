package br.com.pilula.app.model.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;

public class FacebookUtil {
	
	public static void LFMGenerateKeyHash(Context ctx,Class classe) {
		try {
			String sPackage = classe.getPackage().getName();
			Log.e("LFMGenerateKeyHash", sPackage);
			PackageInfo info = ctx.getPackageManager().getPackageInfo(
					sPackage, 
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String hash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
				Log.e("KeyHash:",hash );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	public static String getUserPictureLargeUrl(String fbId) {
		//http://app.cloff.com.br/v3/user/picture/55d4c16dd0e2cf33c235a551
		return "https://graph.facebook.com/"+fbId+"/picture?type=large";
	}

	public static String getUserPictureThumbUrl(String fbId) {
		return "https://graph.facebook.com/"+fbId+"/picture?width=100&height=100";
	}
	*/
	
}
