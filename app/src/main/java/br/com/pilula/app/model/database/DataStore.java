package br.com.pilula.app.model.database;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;

/**
 * Created by lucasferreiramachado on 05/09/15.
 */
public class DataStore {


    public static void setGoogleInfos(Activity loginActivity, String data) {
        save(loginActivity, data, "googleinfos");
    }

    public static String getGoogleInfos(Context act) {
        return load(act, "googleinfos");
    }
    public static void setFaceInfos(Activity loginActivity, String data) {
        save(loginActivity, data, "faceinfos");
    }

    public static String getFaceInfos(Context act) {
        return load(act, "faceinfos");
    }

    public static String load(Context act, String code) {
        try {
            SharedPreferences shared = act.getSharedPreferences(DataStore.class.getPackage().getName(), Context.MODE_PRIVATE);
            if (shared == null) {
                return null;
            }
            return shared.getString(code, null);
        } catch (Exception e) {
            return null;
        }
    }


    public static int save(Context act, String data, String code) {

        try {
            SharedPreferences shared = act.getSharedPreferences(DataStore.class.getPackage().getName(), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = shared.edit();

            editor.putString(code, data);
            editor.commit();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }


    public static void setLastNotifications(FragmentActivity activity, String s) {

    }

    public static String getLastNotifications(FragmentActivity activity) {
        return null;
    }
}
