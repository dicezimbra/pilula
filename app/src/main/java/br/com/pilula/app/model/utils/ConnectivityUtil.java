package br.com.pilula.app.model.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import br.com.pilula.app.model.log.AppLog;

/**
 * Check device's network connectivity and speed 
 * @author emil http://stackoverflow.com/users/220710/emil
 *
 */
public class ConnectivityUtil {

    /**
     * Get the network info
     * @param context
     * @return
     */
    public static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity
     * @param context
     * @return
     */
    public static boolean isConnected(Context context){
        NetworkInfo info = ConnectivityUtil.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network
     * @param context
     * @param type
     * @return
     */
    public static boolean isConnectedWifi(Context context){
        NetworkInfo info = ConnectivityUtil.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network
     * @param context
     * @param type
     * @return
     */
    public static boolean isConnectedMobile(Context context){
        NetworkInfo info = ConnectivityUtil.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public final static int FAST = 3;
    public final static int LOW = 2;
    public final static int POOR = 1;


    /**
     * Check if there is fast connectivity
     * @param context
     * @return
     */
    public static int isConnectedFast(Context context){
        NetworkInfo info = ConnectivityUtil.getNetworkInfo(context);
        if(info != null && info.isConnected()){
            int type = ConnectivityUtil.isConnectionFast(info.getType(), info.getSubtype());
            AppLog.e("isConnectedFast","isConnectedFast type = "+type);
            return type;
        }
        return POOR;
    }

    /**
     * Check if the connection is fast
     * @param type
     * @param subType
     * @return
     */
    public static int isConnectionFast(int type, int subType){
        if(type==ConnectivityManager.TYPE_WIFI){
            return FAST;
        }else if(type==ConnectivityManager.TYPE_MOBILE){
            switch(subType){
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_1xRTT");
                return POOR; // ~ 50-100 kbps
            case TelephonyManager.NETWORK_TYPE_CDMA:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_CDMA");
                return POOR; // ~ 14-64 kbps
            case TelephonyManager.NETWORK_TYPE_EDGE:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_EDGE");
                return POOR; // ~ 50-100 kbps
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_EVDO_0");
                return POOR; // ~ 400-1000 kbps
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_EVDO_A");
                return LOW; // ~ 600-1400 kbps
            case TelephonyManager.NETWORK_TYPE_GPRS:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_GPRS");
                return POOR; // ~ 100 kbps
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_HSDPA");
                return FAST; // ~ 2-14 Mbps
            case TelephonyManager.NETWORK_TYPE_HSPA:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_HSPA");
                return LOW; // ~ 700-1700 kbps
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_HSUPA");
                return FAST; // ~ 1-23 Mbps
            case TelephonyManager.NETWORK_TYPE_UMTS:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_UMTS");
                return LOW; // ~ 400-7000 kbps
            /*
             * Above API level 7, make sure to set android:targetSdkVersion 
             * to appropriate level to use these
             */
            case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_EHRPD");
                return LOW; // ~ 1-2 Mbps
            case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_EVDO_B");
                return FAST; // ~ 5 Mbps
            case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_HSPAP");
                return FAST; // ~ 10-20 Mbps
            case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_IDEN");
                return LOW; // ~25 kbps
            case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_LTE");
                return FAST; // ~ 10+ Mbps
            // Unknown
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                AppLog.e("isConnectionFast","type = NETWORK_TYPE_UNKNOWN");
            default:
                return LOW;
            }
        }else{
            return LOW;
        }
    }

}